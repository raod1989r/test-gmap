import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public searchValue: string;
  private result;
  public lat: string;
  public long: string;
  public mapOptions;
  public marker;

  constructor(private apiService: ApiService) { }
  ngOnInit(): void {
    this.mapOptions = { center: { lat: 30, lng: -77 }, zoom: 14 } as google.maps.MapOptions

    this.marker = {
      position: { lat: 38.9987208, lng: -77.2538699 },
    }
  }

  search() {
    this.apiService.getLocationList(this.searchValue).subscribe(res => {
      let magicKey = res['suggestions'][0]['magicKey'];
      this.apiService.getLocationLatLong(this.searchValue, magicKey).subscribe(result => {
        this.result = result['candidates'][0];
        this.lat = this.result['location']['x'];
        this.long = this.result['location']['y'];

        this.marker = {
          position: { lat: this.lat, lng: this.long }
        }

        this.mapOptions.center.lat = this.lat;
        this.mapOptions.center.lng = this.long;
      })
    })
  }
}
