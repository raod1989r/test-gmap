import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private url: string;
  private latLongUrl: string;
  constructor(private http: HttpClient) { }

  getLocationList(city: string) {
    this.url = `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/suggest?f=json&text=${city}&maxSuggestions=1`
    return this.http.get(this.url);
  }

  getLocationLatLong(city: string, magicKey: string) {
    this.latLongUrl = `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?singleLine=${city}&magicKey=${magicKey}&maxLocations=10&outFields=Match_addr,Place_addr,Type&f=pjson`;
    return this.http.get(this.latLongUrl);
  }
}
